# My project's README



Obtenir AccessDV Linux, procédures
Pour le mode lecture, faire : CTRL + ALT + R
Pour revenir en arrière, faire : ALT + Flèche Gauche

Créer votre clé USB ADVL, mettre ADVL sur un disque dur externe ou sur votre disque interne.
Découvrez nos choix

 

Disons-le tout de suite, nous allons certainement déranger « les puristes » de l’univers Linux !
Nous avons en effet choisi une forme de diffusion et un processus d’installation différents et complètement assumés.

 

A chaque fois que nous rencontrions des acteurs du médico-social, Associations ou structures, la définition du terme Débutant revenait. Souvent, il nous était fait la remarque suivante : l’utilisateur débutant n’est pas un informaticien (et ne souhaite pas pour autant le devenir) . Il est un simple utilisateur en attente de pouvoir réaliser des tâches comme lire son courrier, écrire une lettre, envoyer un courriel, etc... Il ne connaît pas les notions de partition, de bootloader et autres drôleries du jargon informatique.

 

Autre remarque, les éducateurs spécialisés, les instructeurs en Activité de la vie journalière, les instructeurs en locomotion ne sont pas majoritairement des informaticiens. Ils accompagnent certes les personnes Déficientes visuelles mais sont très souvent limités face aux processus d’installation complexes des systèmes d’exploitation.

 

A la fondation IDS LE PHARE (Centre pour Déficients Sensoriels), les chargés des nouvelles technologies nous suggéraient d’avoir un système d’installation le plus simple possible, du genre « système de restauration » que l’on retrouve dans les univers Microsoft ou Apple. Sur un forum dédié à Debian, il avait été suggéré cette même idée avec ce petit commentaire : ce serait quand même cool de pouvoir dire à un noob (débutant en langage de geek barbu) : vas-y tu peux tout casser, tu réinitialises ou tu forces le redémarrage et en 15 min, tu as un système tout neuf !

 
Préalables

 

Vous allez avoir besoin de deux clés USB ou d’une clé USB et d’un disque dur USB externe :

 

    Une clé dite de restauration d’une taille de 8 Go minimum et du type USB 2.0 minimum. Nous allons la nommer dans la procédure qui va suivre : Clé USB BACKUP. Vous garderez par la suite cette clé précieusement.

    Une clé USB 3.0 de 32 Go minimum et permettant des vitesses d’accès de 80 Mb/s minimum pour le confort d’utilisation ou un disque dur externe de 32 Go minimum du type USB 2.0 . Dans la procédure qui va suivre, nous allons nommer cette clé USB 3.0 : Clé USB ADVL.

 
Téléchargement et procédures

 


Lorsque l’on débute en informatique adaptée (approche bien différente de l’informatique « habituelle »), que l’on a jamais utilisé un ordinateur dit adapté, un accompagnement aux techniques et aux bonnes pratiques est nécessaire. Le site internet présente certaines de ces techniques et des bonnes pratiques, elles seront enrichies par la suite, mais cela ne remplacera jamais un accompagnement humain, ce que font les Aidants, les professionnels du secteur, les structures d’accueil et d’accompagnement comme les SAVS (Service d’Accompagnement à la Vie Sociale). Leur rôle est de faire apprendre les techniques de compensation et l’usage des aides techniques. ADVL est une de ces aides techniques.


Mais le rôle des accompagnants n’est pas de se substituer à des informaticiens de métier. Le mode d’installation proposée est le seul qu’ils acceptent de réaliser. Il est important aussi de savoir que l’accès aux services des SAVS ou assimilés est gratuit pour les personnes déficientes visuelles prises en charge.

 

L’installation de AccessDV Linux se fait en trois étapes :

 

1) Télécharger ceci : https://frama.link/advl et https://frama.link/md5
2) Réalisez une clé intermédiaire appelée : Clé ADVL Backup
3) A partir de cette clé, rélisezz une Clé ADVL pour vous, ou installez ADVL sur le disque dur de votre ordinateur, et réalisez autant de clés prêtes à l’emploi que vous voudrez pour vos amis.

Attention, toutes les explications sont sur le document pdf ci-dessous qu’il importe de consulter attentivement, voire à l’imprimer si nécessaire. https://frama.link/advl-pdf N’oubliez pas aussi de consulter l’intégralité de notre site, notamment pour débuter.

 

Ce fichier pdf comporte les étapes suivantes :

 

1 – Créer la clé USB Backup - page 3
2 – Démarrer sur la clé - page 10
3 – Créer la clé USB ADVL - page 14
4 – Installer AccessDV Linux sur le disque dur interne - page 18
5 – Démarrer et utiliser AccessDV Linux- page 23
6 – Avec un BIOS UEFI - page 26
7 - Lancer AccessDV Linux - page 28
8 - Fonds d’écran - page 29
9 - Super_GRUB_Disk - page 33

 

Nous souhaitons que AccessDV Linux vous rende de nombreux services. Nous l’améliorerons au fur et à mesure. Vous pouvez, si vous le souhaitez, vous inscrire, dans le forum, dans notre liste d’utilisateurs privilégiés.
Truc et Astuce, BIOS Legacy

Vous avez créé votre clé USB ADVL ou votre disque dur externe USB ADVL, mais votre ordinateur avec bios UEFI ne vous permet pas de passer en Mode LEGACY pour pouvoir utiliser la clé.

Vous pouvez alors contourner la difficulté en démarrant le PC sur un CD à créer ou une clé USB sur laquelle vous aurez transféré l’iso super_grub_disk 

Cette solution vous permettra d’essayer AccessDV Linux mais est inadapté aux personnes non-voyantes.

